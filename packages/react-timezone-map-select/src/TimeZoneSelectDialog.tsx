import Box from '@mui/material/Box';
import type { ButtonProps } from '@mui/material/Button';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { makeStyles } from '@mui/styles';
import { RawTimeZone, rawTimeZones } from '@vvo/tzdb';
import React, { ReactElement } from 'react';

import { findTimeZone } from '.';
import { DEFAULT_TIME_ZONE_NAME } from './Constant';
import TimeZoneSelect from './TimeZoneSelect';
import WorldMap from './WorldMap';

const useStyles = makeStyles({
  worldMap: {
    marginTop: 10
  }
});

const DEFAULT_TITLE = 'Time Zone';
const DEFAULT_DESCRIPTION = 'Select a time zone in the list or click an area on the map.';
const DEFAULT_BUTTON_LABEL_OK = 'OK';
const DEFAULT_BUTTON_LABEL_CANCEL = 'Cancel';

function DefaultButton(props: ButtonProps): ReactElement {
  const { children, ...rest } = props;
  return (
    <Button variant="contained" {...rest}>
      {children}
    </Button>
  );
}

enum State {
  Closed = 'CLOSED',
  Open = 'OPEN',
}

function findTimeZoneWithDefault(timeZoneName: string): RawTimeZone {
  const timezone = findTimeZone(timeZoneName);
  if (timezone) {
    return timezone;
  } else {
    const defaultTimezone = findTimeZone(DEFAULT_TIME_ZONE_NAME);
    if (defaultTimezone) {
      return defaultTimezone;
    } else {
      // Actually this would not happen. Just for TypeScript checking.
      return rawTimeZones[0];
    }
  }
}

export interface TimeZoneSelectDialogProps {
  open?: boolean;
  onClose?: (newTimeZoneName: string) => void;
  timeZoneName?: string;
  title?: string;
  description?: string;
  buttonLabelOk?: string;
  buttonLabelCancel?: string;
  SubstituteButtonOk?: ((props: ButtonProps) => ReactElement) | string;
  SubstituteButtonCancel?: ((props: ButtonProps) => ReactElement) | string;
  isOkFirstButton?: boolean;
}

export function TimeZoneSelectDialog({
  open = false,
  onClose,
  timeZoneName: givenTimeZoneName = DEFAULT_TIME_ZONE_NAME,
  title = DEFAULT_TITLE,
  description = DEFAULT_DESCRIPTION,
  buttonLabelOk = DEFAULT_BUTTON_LABEL_OK,
  buttonLabelCancel = DEFAULT_BUTTON_LABEL_CANCEL,
  SubstituteButtonOk,
  SubstituteButtonCancel,
  isOkFirstButton = false
}: TimeZoneSelectDialogProps): ReactElement {
  // Sanitize the initial timezone.
  const initialTimeZoneName = findTimeZoneWithDefault(givenTimeZoneName).name;

  const [state, setState] = React.useState(State.Closed);
  const [timeZoneName, setTimeZoneName] = React.useState(initialTimeZoneName);
  const classes = useStyles();

  // Initialize the time zone on opening the dialog.
  React.useEffect(() => {
    if (state === State.Closed && open) {
      setState(State.Open);
      setTimeZoneName(initialTimeZoneName);
    }
  }, [open, state]);

  const handleOK = () => {
    setState(State.Closed);
    if (onClose) {
      onClose(timeZoneName);
    }
  };

  const handleCancel = () => {
    setState(State.Closed);
    if (onClose) {
      onClose(initialTimeZoneName); // revert
    }
  };

  const handleTimeZoneChange = (newTimeZoneName: string) => {
    setTimeZoneName(newTimeZoneName);
  };

  const ButtonOk = SubstituteButtonOk ?? DefaultButton;
  const ButtonCancel = SubstituteButtonCancel ?? DefaultButton;
  const buttons = isOkFirstButton ? (
    <React.Fragment>
      <ButtonOk onClick={handleOK}>
        {buttonLabelOk}
      </ButtonOk>
      <ButtonCancel onClick={handleCancel}>
        {buttonLabelCancel}
      </ButtonCancel>
    </React.Fragment>
  ) : (
    <React.Fragment>
      <ButtonCancel onClick={handleCancel}>
        {buttonLabelCancel}
      </ButtonCancel>
      <ButtonOk onClick={handleOK}>
        {buttonLabelOk}
      </ButtonOk>
    </React.Fragment>
  );

  return (
    <Dialog
      onClose={handleCancel}
      aria-labelledby="time-zone-select-dialog-title"
      open={open}
      maxWidth={'lg'}
    >
      <DialogTitle id="time-zone-select-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <Container>
          <Box>
            <DialogContentText>
              {description}
            </DialogContentText>
          </Box>
          <Box>
            <TimeZoneSelect
              timeZoneName={timeZoneName}
              onChange={handleTimeZoneChange}
            />
          </Box>
          <Box className={classes.worldMap}>
            <WorldMap
              timeZoneName={timeZoneName}
              onChange={handleTimeZoneChange}
            />
          </Box>
        </Container>
      </DialogContent>
      <DialogActions>
        {buttons}
      </DialogActions>
    </Dialog>
  );
}
