import MenuItem from '@mui/material/MenuItem';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { RawTimeZone, rawTimeZones } from '@vvo/tzdb';
import React, { ReactElement } from 'react';

import { convertOffsetInMinutesToString } from './Util';

function getTimeZones(): RawTimeZone[] {
  // Sort timezones according to time offset.
  const timeZones = rawTimeZones.sort((tzA, tzB) => {
    const offsetDiff = tzA.rawOffsetInMinutes - tzB.rawOffsetInMinutes;

    if (offsetDiff === 0) {
      if (tzA.countryName === tzB.countryName) {
        if (tzA.mainCities[0] < tzB.mainCities[0]) {
          return -1;
        } else {
          return 1;
        }
      } else if (tzA.countryName < tzB.countryName) {
        return -1;
      } else {
        return 1;
      }
    } else {
      return offsetDiff;
    }
  });

  return timeZones;
}

interface TimeZoneSelectProps {
  /** Time zone name selected e.g. "Asia/Tokyo" */
  timeZoneName: string;
  /** Called when a timezone is selected. */
  onChange: (timeZoneName: string) => void;
}

const TimeZoneSelect = (props: TimeZoneSelectProps): ReactElement => {
  const timeZones = React.useMemo(getTimeZones, []);
  const handleChange = (event: SelectChangeEvent<string>) => {
    props.onChange(event.target.value);
  };

  return (
    <Select value={props.timeZoneName} onChange={handleChange}>
      {timeZones.map((timeZone) => {
        const title = timeZone.countryName + ' / ' + timeZone.mainCities[0];
        return (
          <MenuItem value={timeZone.name} key={timeZone.name}>
            {convertOffsetInMinutesToString(timeZone.rawOffsetInMinutes) +
              ' : ' +
              title}
          </MenuItem>
        );
      })}
    </Select>
  );
};

export default TimeZoneSelect;
