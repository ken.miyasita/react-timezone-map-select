import '@testing-library/jest-dom';

import { within } from '@testing-library/dom';
import { fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import React from 'react';

import { TimeZoneSelectDialog } from './TimeZoneSelectDialog';

describe('TimeZoneSelectDialog basic', () => {
    test('show nothing by default (open=false)', () => {
        const handleClose = jest.fn();
        render(<TimeZoneSelectDialog onClose={handleClose} />);

        // Nothing is rendered.
        expect(screen.queryByText('OK')).toBeFalsy();
        expect(handleClose).toHaveBeenCalledTimes(0);
    });
    test('return Los Angeles by default', () => {
        const handleClose = jest.fn();
        render(<TimeZoneSelectDialog open={true} onClose={handleClose} />);

        // Click "OK"
        const buttonOk = screen.getByText('OK');
        userEvent.click(buttonOk);

        expect(handleClose).toHaveBeenCalledTimes(1);
        expect(handleClose).toHaveBeenCalledWith('America/Los_Angeles');
    });
    test('return Tokyo by selecting an item in the list', async () => {
        const handleClose = jest.fn();
        render(<TimeZoneSelectDialog open={true} onClose={handleClose} />);

        // Open the list box.
        const select = (screen.queryAllByRole('button'))[0];
        fireEvent.mouseDown(select);
        // Select "Tokyo"
        const listbox = within(screen.getByRole('listbox'));
        const tokyo = listbox.getByText(/tokyo/i);
        userEvent.click(tokyo);
        // Click "OK"
        const buttonOk = screen.getByText('OK');
        userEvent.click(buttonOk);

        expect(handleClose).toHaveBeenCalledTimes(1);
        expect(handleClose).toHaveBeenCalledWith('Asia/Tokyo');
    });
    test('return Los Angeles by cancelling even after selecting Tokyo', async () => {
        const handleClose = jest.fn();
        render(<TimeZoneSelectDialog open={true} onClose={handleClose} />);

        // Open the list box.
        const select = (screen.queryAllByRole('button'))[0];
        fireEvent.mouseDown(select);
        // Select "Tokyo"
        const listbox = within(screen.getByRole('listbox'));
        const tokyo = listbox.getByText(/tokyo/i);
        userEvent.click(tokyo);
        // Click "Cancel"
        const buttonCancel = screen.getByText('Cancel');
        userEvent.click(buttonCancel);

        expect(handleClose).toHaveBeenCalledTimes(1);
        expect(handleClose).toHaveBeenCalledWith('America/Los_Angeles');
    });
    test('return Tokyo by clicking on the map', () => {
        const handleClose = jest.fn();
        render(<TimeZoneSelectDialog open={true} onClose={handleClose} />);

        // Click Tokyo on the map.
        const path = screen.getByTestId('Asia/Tokyo');
        userEvent.click(path);
        // Click "OK"
        const buttonOk = screen.getByText('OK');
        userEvent.click(buttonOk);

        expect(handleClose).toHaveBeenCalledTimes(1);
        expect(handleClose).toHaveBeenCalledWith('Asia/Tokyo');
    });
    test('return Los Angeles by cancelling even after clicking Tokyo on the map', () => {
        const handleClose = jest.fn();
        render(<TimeZoneSelectDialog open={true} onClose={handleClose} />);

        // Click Tokyo on the map.
        const path = screen.getByTestId('Asia/Tokyo');
        userEvent.click(path);
        // Click "Cancel"
        const buttonCancel = screen.getByText('Cancel');
        userEvent.click(buttonCancel);

        expect(handleClose).toHaveBeenCalledTimes(1);
        expect(handleClose).toHaveBeenCalledWith('America/Los_Angeles');
    });
    test('Fallback when invalid timezone name is given', () => {
        const handleClose = jest.fn();
        render(<TimeZoneSelectDialog open={true} timeZoneName="invalid name" onClose={handleClose} />);

        // Fallback to Los Angeles
        expect(screen.queryAllByRole('button')[0]).toHaveTextContent('-08:00 : United States / Los Angeles');
        // Click "OK"
        const buttonOk = screen.getByText('OK');
        userEvent.click(buttonOk);

        expect(handleClose).toHaveBeenCalledTimes(1);
        expect(handleClose).toHaveBeenCalledWith('America/Los_Angeles');
    });
});

describe('TimeZoneSelectDialog customize', () => {
    test('customize text', () => {
        const handleClose = jest.fn();
        render(<TimeZoneSelectDialog open={true} onClose={handleClose}
            timeZoneName="Asia/Tokyo"
            title="custom title"
            description="custom description"
            buttonLabelOk="custom ok"
            buttonLabelCancel="custom cancel"
        />);

        expect(screen.queryAllByRole('button')[0]).toHaveTextContent('+09:00 : Japan / Tokyo');
        expect(screen.getByRole('heading')).toHaveTextContent('custom title');
        expect(screen.getByText('custom description')).toBeTruthy();
        expect(screen.queryAllByRole('button')[1]).toHaveTextContent('custom cancel');
        expect(screen.queryAllByRole('button')[2]).toHaveTextContent('custom ok');
    });
    test('customize button order', () => {
        const handleClose = jest.fn();
        render(<TimeZoneSelectDialog open={true} onClose={handleClose}
            isOkFirstButton={true}
        />);

        expect(screen.queryAllByRole('button')[1]).toHaveTextContent('OK'); // Windows style
        expect(screen.queryAllByRole('button')[2]).toHaveTextContent('Cancel');
    });
    test('customize button component', () => {
        const handleClose = jest.fn();
        render(<TimeZoneSelectDialog open={true} onClose={handleClose}
            SubstituteButtonOk="button"
            SubstituteButtonCancel="button"
        />);

        // Substitute buttons work
        expect(screen.queryAllByRole('button')[1]).toHaveTextContent('Cancel');
        expect(screen.queryAllByRole('button')[2]).toHaveTextContent('OK');

        // You can press OK button
        const buttonOk = screen.getByText('OK');
        userEvent.click(buttonOk);

        expect(handleClose).toHaveBeenCalledTimes(1);
        expect(handleClose).toHaveBeenCalledWith('America/Los_Angeles');
    });
});
