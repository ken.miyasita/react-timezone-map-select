import Box from '@mui/material/Box';
import type { ButtonProps } from '@mui/material/Button';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import React, { ReactElement } from 'react';
import { convertOffsetInMinutesToString, findTimeZone, TimeZoneSelectDialog } from 'react-timezone-map-select';

const DEFAULT_TIME_ZONE_NAME = 'Asia/Tokyo';

function ButtonOk(props: ButtonProps): ReactElement {
  const { children, ...rest } = props;
  return (
    <Button variant="outlined" color="success" {...rest}>
      {children}
    </Button>
  );
}

function App(): ReactElement {
  /** Timezone name */
  const [timezoneName, setTimezoneName] = React.useState(DEFAULT_TIME_ZONE_NAME);

  /** Set true when you open TimeZoneSelectDialog. */
  const [open, setOpen] = React.useState(false);

  /** Called when you press "Open Dialog" button. */
  const handleOpen = React.useCallback(() => {
    setOpen(true);
  }, []);

  /** Called when TimeZoneSelectDialog is closed. */
  const handleClose = React.useCallback((newTimeZoneName: string) => {
    setTimezoneName(newTimeZoneName);
    setOpen(false);
  }, []);

  /** Detailed timezone info */
  const timezone = findTimeZone(timezoneName);
  const timeOffset = timezone ? convertOffsetInMinutesToString(timezone?.rawOffsetInMinutes) : 0;

  return (
    <Container>
      <Box>
        <p>Timezone = {timezoneName}</p>
        <p>Country = {timezone ? timezone.countryName : 'Unknown'}</p>
        <p>Cities = {timezone ? timezone.mainCities.join(', ') : 'Unknown'}</p>
        <p>Offset = {timeOffset}</p>
      </Box>
      <Box>
        <Button onClick={handleOpen} variant="contained" >
          Open Dialog
        </Button>
      </Box>
      <TimeZoneSelectDialog
        open={open}
        timeZoneName={timezoneName}
        onClose={handleClose}
        title="タイムゾーン"
        description="リストの中から選択、もしくは地図上の地点をクリックしてください"
        buttonLabelOk="OK"
        buttonLabelCancel="キャンセル"
        isOkFirstButton={true}           // "OK" comes first (Windows style)
        SubstituteButtonOk={ButtonOk}    // customized react component
        SubstituteButtonCancel="button"  // native HTML tag
      />
    </Container>
  );
}

export default App;
